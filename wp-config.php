<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'dtc' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'nSG|@`i27&NhHXWJ}~J0PgX9Ewf]B7xrx*aD|ABI.$81]pFzRrKuVww%Gdq,MRm+' );
define( 'SECURE_AUTH_KEY',  '$lm2X>Re&tP<JA%5<l]*7ij#j{t,IaHv`bWyypT:gY*_]tfwJ5~_j!_W^CI)5uc)' );
define( 'LOGGED_IN_KEY',    'Y+*:&!}PMiF^0fUHj4!o3JO>%>%wnr>us!_]AWTh=**J&y^b6#S&e9sfYWHIq4wr' );
define( 'NONCE_KEY',        'S`PhD.tE@`)hSkU<SWbqi1jwDm[:$@/bunIU0OChZPn$V)ugk($brb1lo_q&LK4g' );
define( 'AUTH_SALT',        'Z%|lvFdV0!q9gE3S,$g_/INSu7LD 01v61t0+$|sVEsYx:K{E)30:p9M</5|1O!K' );
define( 'SECURE_AUTH_SALT', 'DF1??Ha/=:r=NiX*@` ~NR-S`wy3g#-<N8+7}0S2y6Kw}G4!Z,g|O+)E+|-On?wi' );
define( 'LOGGED_IN_SALT',   ':y[71l)R*AB2X2P)H&qnVz<={cKqE!)DYI>$Uec3q((SKvzZEk^bz9T2/*Hq^hBN' );
define( 'NONCE_SALT',       '+@Q}:94aYk-.(;K&GIz935IOT,SAA%)N12jeGm<aaRJv,&+e!]Lo T{..NYDN< -' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'dtc1_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
